const request = require("supertest");
const { app, server } = require("../../server");

describe("GET /product/:id", () => {
  it("respond with json containing an app", done => {
    request(app)
      .get("/product/62465")
      .set("Accept", "application/json")
      .expect("Content-Type", /json/)
      .expect(200, done);
    server.close();
  });
});
