const AppRepository = require("../infrastructure/repository/app");
const apps = require("../sources/apps.json");
const developers = require("../sources/developers.json");
describe("Test utils", function() {
  it("Should return the correct data", () => {
    expect(new AppRepository().readSource(apps)).toEqual(apps);
  });

  it("Should return the data in the correcto format", () => {
    const developers = [
      {
        id: 23,
        name: "AresGalaxy",
        url: "https://aresgalaxy.io/"
      },
      {
        id: 241,
        name: "Nero AG",
        url: "http://www.nero.com/"
      },
      {
        id: 801,
        name: "VideoLan",
        url: "http://www.videolan.org/"
      }
    ];
    const app = {
      id: "21824",
      developer_id: "23",
      title: "Ares",
      version: "2.4.0",
      url: "http://ares.en.softonic.com",
      short_description: "Fast and unlimited P2P file sharing",
      license: "Free (GPL)",
      thumbnail:
        "https://screenshots.en.sftcdn.net/en/scrn/21000/21824/ares-14-100x100.png",
      rating: 8,
      total_downloads: "4741260",
      compatible: [
        "Windows 2000",
        "Windows XP",
        "Windows Vista",
        "Windows 7",
        "Windows 8"
      ]
    };
    const expected = {
      author_info: {
        name: "AresGalaxy",
        url: "https://aresgalaxy.io/"
      },
      compatible: "Windows 2000|Windows XP|Windows Vista|Windows 7|Windows 8",
      id: "21824",
      license: "Free (GPL)",
      rating: 8,
      short_description: "Fast and unlimited P2P file sharing",
      thumbnail:
        "https://screenshots.en.sftcdn.net/en/scrn/21000/21824/ares-14-100x100.png",
      title: "Ares",
      total_downloads: "4741260",
      url: "http://ares.en.softonic.com",
      version: "2.4.0"
    };
    expect(
      new AppRepository().formatSource(app, developers).formattedReturn()
    ).toEqual(expected);
  });
});
