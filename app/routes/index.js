"use strict";

const controller = require("../controllers/");

function init(server) {
  server.use("/product", controller);
}

module.exports = {
  init: init
};
