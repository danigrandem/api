"use strict";

const AppRepository = require("../infrastructure/repository/app");

const getProductById = async id => {
  const repository = new AppRepository();
  return repository.getProductById(id);
};

module.exports = {
  getProductById
};
