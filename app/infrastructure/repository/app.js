"use strict";

const { getApps, getDevelopers } = require("../index");
const App = require("../../domain/App");

class AppRepository {
  constructor() {
    this.sources = {
      apps: {
        source: getApps(),
        callBackConversion: null
      },
      developers: {
        source: getDevelopers(),
        callBackConversion: null
      }
    };
  }

  readSource(data, convertoToObjectCallBack = null) {
    return convertoToObjectCallBack ? convertoToObjectCallBack(data) : data;
  }

  formatSource(app, developers) {
    return new App({
      ...app,
      developer: developers.filter(
        developer => developer.id == app.developer_id
      )[0]
    });
  }

  async getData() {
    const data = {};
    const keys = Object.keys(this.sources);
    for (let i = 0; i < keys.length; i++) {
      const sourceKey = keys[i];
      const dataSource = this.sources[sourceKey];
      const source = await dataSource.source;
      data[sourceKey] = this.readSource(source, dataSource.callBackConversion);
    }
    return data.apps.map(d => this.formatSource(d, data.developers));
  }

  async getProductById(id) {
    const productsFormatted = await this.getData();
    return productsFormatted.filter(product => product.id == id);
  }
}

module.exports = AppRepository;
