"use strict";

const apps = require("../sources/apps.json");
const developers = require("../sources/developers.json");

const getApps = () => {
  return new Promise((resolve, reject) => {
    resolve(apps);
  });
};

const getDevelopers = () => {
  return new Promise((resolve, reject) => {
    resolve(developers);
  });
};

module.exports = {
  getApps,
  getDevelopers
};
