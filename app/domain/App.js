class App {
  constructor({
    id,
    title,
    version,
    url,
    short_description,
    license,
    thumbnail,
    rating,
    total_downloads,
    compatible,
    developer
  }) {
    this.id = id;
    this.title = title;
    this.version = version;
    this.url = url;
    this.short_description = short_description;
    this.license = license;
    this.thumbnail = thumbnail;
    this.rating = rating;
    this.total_downloads = total_downloads;
    this.compatible = compatible;
    this.developer = developer;
  }

  formattedReturn() {
    const {
      id,
      title,
      version,
      url,
      short_description,
      license,
      thumbnail,
      rating,
      total_downloads,
      compatible,
      developer
    } = this;
    return {
      id,
      author_info: {
        name: developer.name,
        url: developer.url
      },
      title,
      version,
      url,
      short_description,
      license,
      thumbnail,
      rating,
      total_downloads,
      compatible: compatible.join("|")
    };
  }
}

module.exports = App;
