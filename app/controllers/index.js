"use strict";

const express = require("express"),
  service = require("../services/"),
  router = express.Router();

router.get("/:id", async (req, res) => {
  const product = await service.getProductById(req.params.id);
  if (product.length === 0) return res.status(404).send("Not found");
  return res.json(product[0].formattedReturn());
});

module.exports = router;
