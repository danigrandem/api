# API Summary

## Installation steps

### npm install

## Run Project

### npm run dev

## Run tests

### npm run test

## Code Location

### All the code is located in app folder

## Was the first time writing unit testing?

### No, it wasnt my first itme writting unit teting with jest

## What would you do to improve the performance/scalability?

### I would add a cache system as memcahe or redis, maybe I would use GRAPHQL to create a single endpoint to be in charge of putting together all needed requests.

## What would you have done differently if you had had more time

### I would have used typescript and GRAPHQL
