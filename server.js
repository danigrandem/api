const express = require("express");
const bodyParser = require("body-parser");

const app = express();

const port = 8000;

app.use(bodyParser.urlencoded({ extended: true }));

const routes = require("./app/routes");
routes.init(app);

const server = app.listen(port, () => {
  console.log("We are live on " + port);
});

module.exports = { app, server };
